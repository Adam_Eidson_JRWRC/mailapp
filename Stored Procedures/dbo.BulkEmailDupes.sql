SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/30/2019
-- Description:	Returns duplicates to submit to SalesForce for bulk email enagagement.
-- =============================================
--exec MailApp.dbo.BulkEmailDupes
CREATE PROCEDURE [dbo].[BulkEmailDupes]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
with cte_BulkEmailStatTask as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT WhoId, Campaign_Id__c, Opens__c,
First_Open_Date__c,
Last_Open_Date__c,
Clicks__c,
First_Click_Date__c,
Last_Click_Date__c,
Unsubscribed__c,
Bounced__c,
Complained__c,
Id
FROM Task where Type=''Email'' and campaign_id__c !=''''')
)
select *
into #temp
From cte_BulkEmailStatTask



Select whoid, campaign_id__c, count(*) as count_dupes
into #temp_dupes
from #temp t1
 group by whoid, campaign_id__c
 having count(*) >1


; with cte_dupes as (
 select distinct td.whoid,
 td.campaign_id__c,
 t2.id,
 ROW_NUMBER() OVER (Partition By td.whoid, td.campaign_id__c order by td.whoid, td.campaign_id__c) as row_num
 From #temp_dupes td
join #temp t2 on td.Campaign_Id__c = t2.Campaign_Id__c
and td.whoid = t2.whoid 
)
select *
From cte_dupes
where row_num >1

END
GO
GRANT EXECUTE ON  [dbo].[BulkEmailDupes] TO [ETL_User]
GO
