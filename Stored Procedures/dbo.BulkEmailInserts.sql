SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/28/2019
-- Description:	Returns updates to submit to SalesForce for bulk email enagagement.
-- =============================================
--exec MailApp.dbo.BulkEmailInserts @AccountName = 'ER BD/RIA Contacts'
CREATE PROCEDURE [dbo].[BulkEmailInserts]
@AccountName varchar(250) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--if @AccountName != 'ER BD/RIA Contacts'
--BEGIN
select *
into #Temp_Contact
From OPENQUERY(SALESFORCEDEVART,'select Id, Email from Contact where Email != '''' and isPersonAccount = 0')


select *
into #TempBulkEmail
From [MailApp].[dbo].[vSalesForceContactBulkEmailStats]

--select *
--into #Temp_Client
--From OPENQUERY(SALESFORCEDEVART,'select Id, PersonEmail from Account where IsPersonAccount = ''True'' and PersonEmail != ''''')

SELECT distinct EmailId.Id,
ce.ToEmailAddress,
ce.CampaignId,
ce.ArchiveUrl,
ce.Subject,
convert(nvarchar(30),ce.SendTime,127) as SendTime,
ce.FromName,
ce.ReplyToAddress,
ce.Delivered,
ce.Opens,
convert(nvarchar(30),ce.FirstOpenDate,127) as FirstOpenDate,
convert(nvarchar(30),ce.LastOpenDate,127) as LastOpenDate,
ce.Clicks,
ce.ClickedUrls,
convert(nvarchar(30),ce.FirstClickDate,127) as FirstClickDate,
convert(nvarchar(30),ce.LastClickDate,127) as LastClickDate,
ce.Bounced,
ce.Unsubscribed,
ce.Complained
  FROM #Temp_Contact EmailId
  join MailApp.STAGE.CampaignEngagement ce on ce.ToEmailAddress = EmailId.Email
												and ce.AccountName = @AccountName
 left join #TempBulkEmail v on ce.ToEmailAddress = v.Email
											and ce.CampaignId = v.campaign_id__c
--where ce.sfid is null
where v.WhoId is null
--END
--ELSE
--BEGIN

--select *
--From MailApp.dbo.[ER BD/RIA Contact Insert]
--END
--union 

--SELECT distinct EmailId.Id,
--ce.ToEmailAddress,
--ce.CampaignId,
--ce.ArchiveUrl,
--ce.Subject,
--convert(nvarchar(30),ce.SendTime,127) as SendTime,
--ce.FromName,
--ce.ReplyToAddress,
--ce.Delivered,
--ce.Opens,
--convert(nvarchar(30),ce.FirstOpenDate,127) as FirstOpenDate,
--convert(nvarchar(30),ce.LastOpenDate,127) as LastOpenDate,
--ce.Clicks,
--ce.ClickedUrls,
--convert(nvarchar(30),ce.FirstClickDate,127) as FirstClickDate,
--convert(nvarchar(30),ce.LastClickDate,127) as LastClickDate,
--ce.Bounced,
--ce.Unsubscribed,
--ce.Complained
--  FROM #Temp_Client EmailId
--  join MailApp.STAGE.CampaignEngagement ce on ce.ToEmailAddress = EmailId.PersonEmail
--												and ce.AccountName = @AccountName
-- left join [MailApp].[dbo].[vSalesForceClientBulkEmailStats] v on ce.ToEmailAddress = v.Email
--											and ce.CampaignId = v.campaign_id__c
----where ce.sfid is null
--where v.WhoId is null

END
GO
GRANT EXECUTE ON  [dbo].[BulkEmailInserts] TO [ETL_User]
GO
