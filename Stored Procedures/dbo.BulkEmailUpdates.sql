SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 10/28/2019
-- Description:	Returns updates to submit to SalesForce for bulk email enagagement.
-- =============================================
--exec MailApp.dbo.BulkEmailUpdates
CREATE PROCEDURE [dbo].[BulkEmailUpdates]
@AccountName varchar(250)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  distinct ce.Opens,
convert(nvarchar(30),ce.FirstOpenDate,127) as FirstOpenDate,
convert(nvarchar(30),ce.LastOpenDate,127) as LastOpenDate,
ce.Clicks,
ce.ClickedUrls,
convert(nvarchar(30),ce.FirstClickDate,127) as FirstClickDate,
convert(nvarchar(30),ce.LastClickDate,127) as LastClickDate,
ce.Complained,
v.Id,
ce.Bounced,
ce.Unsubscribed
  FROM [MailApp].[dbo].[vSalesForceContactBulkEmailStats] v
  join MailApp.STAGE.CampaignEngagement ce on ce.ToEmailAddress = v.Email
											and ce.CampaignId = v.campaign_id__c
											and ce.AccountName = @AccountName
--where ce.sfid is null
where v.Opens__c != ce.Opens
or v.Clicks__c != ce.Clicks
or v.Complained__c != ce.Complained
END
GO
GRANT EXECUTE ON  [dbo].[BulkEmailUpdates] TO [ETL_User]
GO
