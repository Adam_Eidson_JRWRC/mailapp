SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Adam Eidson
-- Create date: 2/4/2019
-- Description:	Returns updates to submit to SalesForce for bulk email enagagement for RIA Farm.
-- =============================================
--exec MailApp.dbo.BulkEmailInserts-RIAFarm
CREATE PROCEDURE [dbo].[BulkEmailInserts-RIAFarm]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select *
into #Temp_Contact
From OPENQUERY(SALESFORCEDEVART,'select Id, Email from Contact where Email != '''' and isPersonAccount = 0')


SELECT distinct EmailId.Id,
ce.ToEmailAddress,
ce.CampaignId,
ce.ArchiveUrl,
ce.Subject,
convert(nvarchar(30),ce.SendTime,127) as SendTime,
ce.FromName,
ce.ReplyToAddress,
ce.Delivered,
ce.Opens,
convert(nvarchar(30),ce.FirstOpenDate,127) as FirstOpenDate,
convert(nvarchar(30),ce.LastOpenDate,127) as LastOpenDate,
ce.Clicks,
ce.ClickedUrls,
convert(nvarchar(30),ce.FirstClickDate,127) as FirstClickDate,
convert(nvarchar(30),ce.LastClickDate,127) as LastClickDate,
ce.Bounced,
ce.Unsubscribed,
ce.Complained
  FROM #Temp_Contact EmailId
  join MailApp.STAGE.[CampaignEngagement-RIAFarm] ce on ce.ToEmailAddress = EmailId.Email
 left join [MailApp].[dbo].[vSalesForceContactBulkEmailStats] v on ce.ToEmailAddress = v.Email
											and ce.CampaignId = v.campaign_id__c
--where ce.sfid is null
where v.WhoId is null

END
GO
GRANT EXECUTE ON  [dbo].[BulkEmailInserts-RIAFarm] TO [ETL_User]
GO
