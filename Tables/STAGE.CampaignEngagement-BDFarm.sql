CREATE TABLE [STAGE].[CampaignEngagement-BDFarm]
(
[AccountName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToEmailAddress] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampaignId] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ArchiveUrl] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Subject] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SendTime] [datetime] NOT NULL,
[FromName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ToName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReplyToAddress] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Delivered] [bit] NOT NULL,
[Opens] [int] NOT NULL,
[FirstOpenDate] [datetime] NULL,
[LastOpenDate] [datetime] NULL,
[Clicks] [int] NOT NULL,
[ClickedUrls] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstClickDate] [datetime] NULL,
[LastClickDate] [datetime] NULL,
[Bounced] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Unsubscribed] [bit] NULL,
[Complained] [bit] NULL,
[SFID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
