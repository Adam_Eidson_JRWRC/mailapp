SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [dbo].[vSalesForceContactBulkEmailStats]
as
with cte_Contact as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT Email, Id FROM Contact where Email is not null')
), cte_BulkEmailStatTask as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT WhoId, Campaign_Id__c, Opens__c,
First_Open_Date__c,
Last_Open_Date__c,
Clicks__c,
First_Click_Date__c,
Last_Click_Date__c,
Unsubscribed__c,
Bounced__c,
Complained__c,
Id
FROM Task where Type=''Email'' and campaign_id__c !=''''
and createdDate > ''2021-07-15 11:52:23.0000000''
')
)

select c.Id as Contact_ID, 
c.email,
best.*
From cte_Contact c
left join cte_BulkEmailStatTask best on best.WhoId = c.Id
GO
