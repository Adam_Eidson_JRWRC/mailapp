SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





Create VIEW [dbo].[vSalesForceClientBulkEmailStats]
as
with cte_Client as (
select *
From OPENQUERY(SALESFORCEDEVART,'select Id, PersonEmail from Account where IsPersonAccount = ''True'' and PersonEmail != ''''')
), cte_BulkEmailStatTask as (
SELECT * FROM OPENQUERY(SALESFORCEDEVART,'SELECT WhoId, Campaign_Id__c, Opens__c,
First_Open_Date__c,
Last_Open_Date__c,
Clicks__c,
First_Click_Date__c,
Last_Click_Date__c,
Unsubscribed__c,
Bounced__c,
Complained__c,
Id
FROM Task where Type=''Email'' and campaign_id__c !=''''')
)

select c.Id as Contact_ID, 
c.PersonEmail as email,
best.*
From cte_Client c
left join cte_BulkEmailStatTask best on best.WhoId = c.Id
GO
